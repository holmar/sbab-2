package se.markus.sbab.integration.trafiklab.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Line {

    @JsonProperty("LineNumber")
    private String lineId;

    @JsonProperty("DirectionCode")
    private String directionId;

    @JsonProperty("JourneyPatternPointNumber")
    private String stopPointId;

    public Line() {
    }

    public Line(String lineId, String directionId, String stopPointId) {
        this.lineId = lineId;
        this.directionId = directionId;
        this.stopPointId = stopPointId;
    }

    public String getLineId() {
        return lineId;
    }

    public String getDirectionId() {
        return directionId;
    }

    public String getStopPointId() {
        return stopPointId;
    }

}
