package se.markus.sbab.integration.trafiklab.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Stop {

	@JsonProperty("StopPointNumber")
	private String stopPointNumber;

	@JsonProperty("StopPointName")
	private String stopPointName;

	@JsonProperty("StopAreaNumber")
	private String stopAreaNumber;

	@JsonProperty("LocationNorthingCoordinate")
	private String locationNorthingCoordinate;

	@JsonProperty("LocationEastingCoordinate")
	private String locationEastingCoordinate;

	@JsonProperty("ZoneShortName")
	private String zoneShortName;

	@JsonProperty("StopAreaTypeCode")
	private String stopAreaTypeCode;

	@JsonProperty("LastModifiedUtcDateTime")
	private String lastModifiedUtcDateTime;

	@JsonProperty("ExistsFromDate")
	private String existsFromDate;

	public Stop() {
	}
	public Stop(
			final String stopPointNumber,
			final String stopPointName,
			final String stopAreaNumber,
			final String locationNorthingCoordinate,
			final String locationEastingCoordinate,
			final String zoneShortName,
			final String stopAreaTypeCode,
			final String lastModifiedUtcDateTime,
			final String existsFromDate) {
		this.stopPointNumber = stopPointNumber;
		this.stopPointName = stopPointName;
		this.stopAreaNumber = stopAreaNumber;
		this.locationNorthingCoordinate = locationNorthingCoordinate;
		this.locationEastingCoordinate = locationEastingCoordinate;
		this.zoneShortName = zoneShortName;
		this.stopAreaTypeCode = stopAreaTypeCode;
		this.lastModifiedUtcDateTime = lastModifiedUtcDateTime;
		this.existsFromDate = existsFromDate;
	}

	public String getStopPointNumber() {
		return stopPointNumber;
	}

	public String getStopPointName() {
		return stopPointName;
	}

	public String getStopAreaNumber() {
		return stopAreaNumber;
	}

	public String getLocationNorthingCoordinate() {
		return locationNorthingCoordinate;
	}

	public String getLocationEastingCoordinate() {
		return locationEastingCoordinate;
	}

	public String getZoneShortName() {
		return zoneShortName;
	}

	public String getStopAreaTypeCode() {
		return stopAreaTypeCode;
	}

	public String getLastModifiedUtcDateTime() {
		return lastModifiedUtcDateTime;
	}

	public String getExistsFromDate() {
		return existsFromDate;
	}
}
