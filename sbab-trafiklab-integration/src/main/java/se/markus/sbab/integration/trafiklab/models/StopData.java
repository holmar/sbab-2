package se.markus.sbab.integration.trafiklab.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StopData {

	@JsonProperty("Version")
	private String version;

	@JsonProperty("Type")
	private String type;

	@JsonProperty("Result")
	private List<Stop> stops;

	public StopData() {
	}

	public StopData(final String version, final String type, final List<Stop> stops) {
		this.version = version;
		this.type = type;
		this.stops = stops;
	}

	public String getVersion() {
		return version;
	}

	public String getType() {
		return type;
	}

	public List<Stop> getStops() {
		return stops;
	}
}
