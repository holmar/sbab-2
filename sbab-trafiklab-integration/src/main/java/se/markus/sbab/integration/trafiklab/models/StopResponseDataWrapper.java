package se.markus.sbab.integration.trafiklab.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StopResponseDataWrapper {

	@JsonProperty("ResponseData")
	private StopData responseData;

	public StopResponseDataWrapper() {
	}

	public StopResponseDataWrapper(StopData responseData) {
		this.responseData = responseData;
	}

	public StopData getResponseData() {
		return responseData;
	}
}
