package se.markus.sbab.integration.trafiklab;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LineDataProperties {

	private final String endpoint;
	private final String token;

	public LineDataProperties(
			@Value("${trafiklab.api.lineData.endpoint}") final String endpoint,
			@Value("${trafiklab.api.lineData.token}") final String token) {
		this.endpoint = endpoint;
		this.token = token;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public String getToken() {
		return token;
	}
}
