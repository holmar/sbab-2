package se.markus.sbab.integration.trafiklab;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import se.markus.sbab.core.models.BusStop;
import se.markus.sbab.core.services.LineDataService;
import se.markus.sbab.integration.trafiklab.models.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LineDataServiceImpl implements LineDataService {

    private final RestTemplate restTemplate;
    private final LineDataProperties lineDataProperties;

    public LineDataServiceImpl(final RestTemplate restTemplate, final LineDataProperties lineDataProperties ) {
        this.restTemplate = restTemplate;
        this.lineDataProperties = lineDataProperties;
    }

    @Override
    public List<BusStop> getStopsAndLines() {

        List<Line> lines = getLines();
        List<Stop> stops = getStops();
        HashMap<String, List<Stop>> stopsAndLines = new HashMap<>();
        List<BusStop> result = new ArrayList<>();

        for (Line line: lines) {
            List<Stop> lineStops = filterStopsWithStopPoints(stops, line);
            stopsAndLines = addStopsToLineId(stopsAndLines, line.getLineId(), lineStops);
        }

        stopsAndLines.forEach((key, value) -> {
            for (Stop s: value) {
                BusStop busStop = new BusStop(
                        Integer.parseInt(s.getStopPointNumber()),
                        s.getStopPointName(),
                        Integer.parseInt(s.getStopAreaNumber()),
                        Integer.parseInt(key));
                result.add(busStop);

            }
        });
        return result;
    }

    private HashMap<String, List<Stop>> addStopsToLineId(HashMap<String, List<Stop>> stopsAndLines, String key, List<Stop> lineStops) {
        for (Stop stop: lineStops) {
            if (stopsAndLines.containsKey(key)) {
                List<Stop> oldStop = stopsAndLines.get(key);
                oldStop.add(stop);
            } else {
                List<Stop> newStop = new ArrayList<>();
                newStop.add(stop);
                stopsAndLines.put(key, newStop);
            }
        }

        return stopsAndLines;
    }

    private List<Stop> filterStopsWithStopPoints(List<Stop> stops, Line line) {
        return stops.stream().filter(f -> {
                    return f.getStopPointNumber().equals(line.getStopPointId());
                }).collect(Collectors.toList());
    }

    private List<Stop> getStops() {
        StopResponseDataWrapper stopWrapper = restTemplate.getForObject(lineDataProperties.getEndpoint() + "&model=Stop", StopResponseDataWrapper.class);
        return stopWrapper.getResponseData().getStops();
    }

    private List<Line> getLines() {
        LineResponseDataWrapper wrapper = restTemplate.getForObject(lineDataProperties.getEndpoint() + "&model=jour", LineResponseDataWrapper.class);
        return wrapper.getResponseData().getLines();
    }
}
