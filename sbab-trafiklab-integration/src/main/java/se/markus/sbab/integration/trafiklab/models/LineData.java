package se.markus.sbab.integration.trafiklab.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LineData {

    @JsonProperty("Version")
    private String version;

    @JsonProperty("Type")
    private String type;

    @JsonProperty("Result")
    private List<Line> lines;

    public LineData() {
    }

    public LineData(final String version, final String type, final List<Line> lines) {
        this.version = version;
        this.type = type;
        this.lines = lines;
    }

    public String getVersion() {
        return version;
    }

    public String getType() {
        return type;
    }

    public List<Line> getLines() {
        return lines;
    }
}
