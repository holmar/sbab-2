package se.markus.sbab.integration.trafiklab.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class LineResponseDataWrapper {

    @JsonProperty("ResponseData")
    private LineData responseData;

    public LineResponseDataWrapper() {
    }

    public LineResponseDataWrapper(LineData responseData) {
        this.responseData = responseData;
    }

    public LineData getResponseData() {
        return responseData;
    }
}
