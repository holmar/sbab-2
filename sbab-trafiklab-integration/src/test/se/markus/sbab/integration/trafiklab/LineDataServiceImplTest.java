package se.markus.sbab.integration.trafiklab;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;
import se.markus.sbab.core.models.BusStop;
import se.markus.sbab.integration.trafiklab.models.*;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;


@SpringBootTest
class LineDataServiceImplTest {

    @Mock
    private LineDataProperties lineDataProperties;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private LineDataServiceImpl lineDataService;


    @Test
    public void test_getStops() {

        setupMockForLines();
        setupMockForStops();

        List<BusStop> lines = lineDataService.getStopsAndLines();
        assertFalse(lines.isEmpty());

        assertEquals(3, lines.size());

        BusStop busStop = lines.get(0);
        assertEquals("Test 1", busStop.getStopName());

    }

    private void setupMockForLines() {
        Line line1 = new Line("1", "1", "100");
        Line line2 = new Line("2", "1", "200");
        Line line3 = new Line("2", "1", "300");

        List<Line> lines = new ArrayList<>();
        lines.add(line1);
        lines.add(line2);
        lines.add(line3);

        LineData lineData = new LineData("", "", lines);
        LineResponseDataWrapper lineWrapper = new LineResponseDataWrapper(lineData);

        Mockito.when(restTemplate.getForObject(lineDataProperties.getEndpoint() + "&model=jour", LineResponseDataWrapper.class))
                .thenReturn(lineWrapper);
    }

    private void setupMockForStops() {
        Stop stop = new Stop("100",
                "Test 1",
                "1",
                "123",
                "666",
                "A", "" +
                "BUS",
                "" , "");

        Stop stop2 = new Stop("100",
                "Test 2",
                "1",
                "123",
                "777",
                "A", "" +
                "BUS",
                "" , "");

        Stop stop3 = new Stop("200",
                "Test 3",
                "2",
                "123",
                "777",
                "A", "" +
                "BUS",
                "" , "");
        List<Stop> stops = new ArrayList<>();
        stops.add(stop);
        stops.add(stop2);
        stops.add(stop3);

        StopData stopData = new StopData("","", stops);
        StopResponseDataWrapper stopWrapper = new StopResponseDataWrapper(stopData);

        Mockito.when(restTemplate.getForObject(lineDataProperties.getEndpoint() + "&model=Stop", StopResponseDataWrapper.class))
                .thenReturn(stopWrapper);
    }
}