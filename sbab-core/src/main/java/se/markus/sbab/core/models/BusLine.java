package se.markus.sbab.core.models;

import java.util.List;

public class BusLine {


    private final Long id;

    private final int lineNumber;

    private final int destinationId;

    private final int stopPointId;

    private final List<BusStop> stops;

    public BusLine(Long id, int lineNumber, int destinationId, int stopPointId, List<BusStop> stops) {
        this.id = id;
        this.lineNumber = lineNumber;
        this.destinationId = destinationId;
        this.stopPointId = stopPointId;
        this.stops = stops;
    }

    public Long getId() {
        return id;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public int getDestinationId() {
        return destinationId;
    }

    public int getStopPointId() {
        return stopPointId;
    }

    public List<BusStop> getStops() {
        return stops;
    }

    @Override
    public String toString() {
        return "BusLine{" +
                "id=" + id +
                ", \\n lineNumber=" + lineNumber +
                ", \\n destinationId=" + destinationId +
                ", \\n stopPointId=" + stopPointId +
                ", \\n stops=" + stops +
                '}';
    }
}
