package se.markus.sbab.core.services;

import se.markus.sbab.core.models.BusStop;

import java.util.List;
import java.util.Map;

public interface BusService {


    List<BusStop> getStops(int lineNumber);

    List<BusStop> getStops();

    Map<Integer, Long> getTop10BusLines();

    void saveAll(List<BusStop> stops);

}
