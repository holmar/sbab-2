package se.markus.sbab.core.services;

import se.markus.sbab.core.models.BusStop;
import java.util.List;

public interface LineDataService {

	List<BusStop> getStopsAndLines();
}
