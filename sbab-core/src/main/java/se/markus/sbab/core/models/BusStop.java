package se.markus.sbab.core.models;

public class BusStop {


    private final int stopId;

    private final String stopName;

    private final int areaNumber;

    private final int lineId;

    public BusStop(final int stopId, final String stopName, final int areaNumber, final int lineId) {
        this.stopId = stopId;
        this.stopName = stopName;
        this.areaNumber = areaNumber;
        this.lineId = lineId;
    }


    public int getStopId() {
        return stopId;
    }


    public String getStopName() {
        return stopName;
    }


    public int getAreaNumber() {
        return areaNumber;
    }

    public int getLineId() {
        return lineId;
    }

    @Override
    public String toString() {
        return "BusStop{" +
                ", stopId=" + stopId +
                ", stopName='" + stopName + '\'' +
                ", areaNumber=" + areaNumber +
                ", lineId=" + lineId +
                '}';
    }
}
