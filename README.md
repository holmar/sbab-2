# Table of Content
* [Challenge](#challenge)
    * [Language](#language)
    * [Delivery](#delivery)
* [How to use](#how-to-use)
* [Testing](#testing)

---
# Challenge
The task is to implement an API for potential clients. The API will have minimum 2 endpoints.
One end-point should return the **top 10 bus lines in Stockholm area with most bus stops on
their route**. The other endpoint should return a **list of every bus stop of the bus line
provided**. There is no requirement on how the bus stops are sorted.

To accomplish the task please use the Trafiklab’s open API (https://www.trafiklab.se). You
can find more information about the specific API at the documentation page: http://www.trafiklab.se/api/sl-hallplatser-och-linjer-2.

You can register your own account at Trafiklab and obtain an API key.

**The data should be automatically gathered from API for each run of the application.**

## Language
We would like you to implement the application with Java. You are free to choose the
version of Java. You can use external libraries if you would like to.

## Delivery
We would like you to send your code in advance so we can review it before we invite you to
discuss the solution.
The delivery of the solution should include the source code, the build script and the
instructions to run the application in MacOS/Unix or Windows.

# TODO: 
* Fixa package mvn
* tester 

# How to use

In the root folder of the project run `mvn clean install`
after that if you can run `java -jar sbab-application/target/sbab-application-0.0.1-SNAPSHOT.jar`
now the backend is up and running.


# Testing
list of every bus stop of the bus line provided
 
```bash
$ curl http://localhost:8080/stops/605
```

top 10 bus lines in Stockholm area with most bus stops on
their route
```bash
$ curl http://localhost:8080/lines/toplist
```

list of every bus stop
```bash
$ curl http://localhost:8080/stops
```

You can find a postman collection file under the folder postman. if you import it in to postman
all endpoints will be there for testing.