package se.markus.sbab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import se.markus.sbab.core.models.BusStop;
import se.markus.sbab.core.services.BusService;
import se.markus.sbab.core.services.LineDataService;

import java.util.List;

@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	private BusService busService;

	@Autowired
	private LineDataService lineDataService;


	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public RestTemplate restTemplate(final RestTemplateBuilder builder) {
		return builder.build();
	}

	@Override
	public void run(String... args) throws Exception {

		List<BusStop> stops = lineDataService.getStopsAndLines();

		System.out.println("#### stops: " + stops.toString());
		busService.saveAll(stops);
	}
}