package se.markus.sbab;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.markus.sbab.core.models.BusStop;
import se.markus.sbab.core.services.BusService;

import java.util.List;
import java.util.Map;

@RestController
public class BusResource {

    private final BusService busService;

    public BusResource(final BusService busService) {
        this.busService = busService;
    }

    @RequestMapping("/stops")
    public List<BusStop> getStops() {
        List<BusStop> lines = busService.getStops();
        return lines;
    }

    @RequestMapping("/stops/{lineNumber}")
    public List<BusStop> getStops(@PathVariable("lineNumber") int lineNumber) {
        List<BusStop> lines = busService.getStops(lineNumber);
        return lines;
    }

    @RequestMapping("/lines/toplist")
    public Map<Integer, Long> getTopList() {
        return busService.getTop10BusLines();

    }
}
