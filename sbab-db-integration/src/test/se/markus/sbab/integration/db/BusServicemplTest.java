package se.markus.sbab.integration.db;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import se.markus.sbab.core.models.BusStop;
import se.markus.sbab.integration.db.models.BusStopDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class BusServicemplTest {

    @Mock
    private BusStopRepository busStopRepository;

    @InjectMocks
    private BusServicempl busService;

    @BeforeEach
    private void setup() {

        BusStopDAO stop = new BusStopDAO(100, "Line 1 Stop1", 1, 1);
        BusStopDAO stop2 = new BusStopDAO(200, "Line 1 Stop2", 1, 1);
        BusStopDAO stop3 = new BusStopDAO(300, "Line 1 Stop3", 1, 1);

        BusStopDAO stop4 = new BusStopDAO(111, "Line 2 Stop1", 2, 2);
        BusStopDAO stop5 = new BusStopDAO(222, "Line 2 Stop2", 2, 2);
        BusStopDAO stop6 = new BusStopDAO(333, "Line 2 Stop3", 2, 2);
        BusStopDAO stop7 = new BusStopDAO(333, "Line 2 Stop4", 2, 2);

        BusStopDAO stop8 = new BusStopDAO(7777, "Line 3 Stop2", 3, 3);
        BusStopDAO stop9 = new BusStopDAO(12323, "Line 3 Stop3", 3, 3);
        BusStopDAO stop10 = new BusStopDAO(656, "Line 3 Stop4", 3, 3);
        BusStopDAO stop11 = new BusStopDAO(565656, "Line 3 Stop4", 3, 3);

        List<BusStopDAO> stops = new ArrayList<>();
        stops.add(stop);
        stops.add(stop2);
        stops.add(stop3);
        stops.add(stop4);
        stops.add(stop5);
        stops.add(stop6);
        stops.add(stop7);
        stops.add(stop8);
        stops.add(stop9);
        stops.add(stop10);
        stops.add(stop11);

        List<BusStopDAO> stopsForLine1 = new ArrayList<>();
        stopsForLine1.add(stop8);
        stopsForLine1.add(stop9);
        stopsForLine1.add(stop10);
        stopsForLine1.add(stop11);

        when(busStopRepository.findAll()).thenReturn(stops);
        when(busStopRepository.findAllByLineId(3)).thenReturn(stopsForLine1);
    }

    @Test
    void test_Stops() {
        List<BusStop> stops = busService.getStops();
        assertEquals(11, stops.size());

        List<BusStop> stopsForLineOne = stops.stream().filter(stop -> {
            return stop.getLineId() == 1;
        }).collect(Collectors.toList());
        assertEquals(3, stopsForLineOne.size());

        List<BusStop> stopsForLinetwo = stops.stream().filter(stop -> {
            return stop.getLineId() == 2;
        }).collect(Collectors.toList());

        assertEquals(4, stopsForLinetwo.size());
    }

    @Test
    void getTop10BusLines() {
        Map<Integer, Long> toplist = busService.getTop10BusLines();
        int lineId = 2;
        int expectedStopCount = 4;
        assertEquals(3, toplist.size());
        assertEquals(expectedStopCount, toplist.get(lineId));

    }

    @Test
    void test_StopsWithLineNumber() {
        List<BusStop> stops = busService.getStops(3);
        int expectedStopCount = 4;
        assertEquals(expectedStopCount, stops.size());
    }
}