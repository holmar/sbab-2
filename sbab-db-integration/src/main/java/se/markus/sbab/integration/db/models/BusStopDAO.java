package se.markus.sbab.integration.db.models;

import se.markus.sbab.core.models.BusLine;
import se.markus.sbab.core.models.BusStop;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="bus_stops")
public class BusStopDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int stopId;
    private String stopName;
    private int areaNumber;
    private int lineId;

    public BusStopDAO() {
    }

    public BusStopDAO(int stopId, String stopName, int areaNumber, int lineId) {
        this.stopId = stopId;
        this.stopName = stopName;
        this.areaNumber = areaNumber;
        this.lineId = lineId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStopId() {
        return stopId;
    }

    public void setStopId(int stopId) {
        this.stopId = stopId;
    }

    public String getStopName() {
        return stopName;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public int getAreaNumber() {
        return areaNumber;
    }

    public void setAreaNumber(int areaNumber) {
        this.areaNumber = areaNumber;
    }

    public int getLineId() {
        return lineId;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

    public BusStop toCore() {
        return new BusStop(this.stopId, this.stopName, this.stopId, this.lineId);
    }
}
