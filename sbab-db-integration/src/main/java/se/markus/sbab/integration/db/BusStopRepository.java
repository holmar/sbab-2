package se.markus.sbab.integration.db;

import org.springframework.data.repository.CrudRepository;
import se.markus.sbab.integration.db.models.BusStopDAO;

import java.util.List;

public interface BusStopRepository extends CrudRepository<BusStopDAO, Long> {

    List<BusStopDAO> findAllByLineId(int lineId);

    @Override
    List<BusStopDAO> findAll();
}
