package se.markus.sbab.integration.db;

import org.springframework.stereotype.Component;
import se.markus.sbab.core.models.BusStop;
import se.markus.sbab.core.services.BusService;
import se.markus.sbab.integration.db.models.BusStopDAO;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class BusServicempl implements BusService {

    private final int TOPLIST_LIMIT = 10;

    private final BusStopRepository busStopRepository;

    public BusServicempl(final BusStopRepository busStopRepository) {
        this.busStopRepository = busStopRepository;
    }

    @Override
    public List<BusStop> getStops() {
        List<BusStopDAO> stops = busStopRepository.findAll();
        return stops.stream().map(stop -> stop.toCore()).collect(Collectors.toList());
    }

    @Override
    public Map<Integer, Long> getTop10BusLines() {
        List<BusStopDAO> stops = busStopRepository.findAll();

        Map<Integer, Long> toplist = stops.stream().collect(Collectors.groupingBy(BusStopDAO::getLineId, Collectors.counting()))
                .entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(TOPLIST_LIMIT)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        return toplist;
    }

    @Override
    public List<BusStop> getStops(int lineNumber) {
        List<BusStopDAO> stops = busStopRepository.findAllByLineId(lineNumber);
        return stops.stream().map(stop -> stop.toCore()).collect(Collectors.toList());
    }

    @Override
    public void saveAll(List<BusStop> stops) {

        stops.forEach(stop -> {
            busStopRepository.save(new BusStopDAO(
                    stop.getStopId(),
                    stop.getStopName(),
                    stop.getAreaNumber(),
                    stop.getLineId()));
        });
    }
}
