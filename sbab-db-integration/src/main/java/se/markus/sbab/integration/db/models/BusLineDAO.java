package se.markus.sbab.integration.db.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="bus_line")
public class BusLineDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int lineNumber;

    private int destinationId;

    private int stopPointId;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BusStopDAO> stops;

    public BusLineDAO() {
    }

    public BusLineDAO(int lineNumber, int destinationId, int stopPointId, List<BusStopDAO> stops) {
        this.lineNumber = lineNumber;
        this.destinationId = destinationId;
        this.stopPointId = stopPointId;
        this.stops = stops;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
    }

    public int getStopPointId() {
        return stopPointId;
    }

    public void setStopPointId(int stopPointId) {
        this.stopPointId = stopPointId;
    }

    public List<BusStopDAO> getStops() {
        return stops;
    }

    public void setStops(List<BusStopDAO> stops) {
        this.stops = stops;
    }
}
